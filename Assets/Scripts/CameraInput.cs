using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraInput : MonoBehaviour
{
    [SerializeField]
    private float m_speed;

    // Update is called once per frame
    void Update()
    {
        Vector3 InputVector = Vector3.zero;
        if (Input.GetKey(KeyCode.W))
        {
            InputVector += new Vector3(0, 0, 1);
        }
        if (Input.GetKey(KeyCode.S))
        {
            InputVector -= new Vector3(0, 0, 1);
        }
        if (Input.GetKey(KeyCode.A))
        {
            InputVector -= new Vector3(1, 0, 0);
        }
        if (Input.GetKey(KeyCode.D))
        {
            InputVector += new Vector3(1, 0, 0);
        }
        transform.position += InputVector * Time.deltaTime * m_speed;
    }
}
