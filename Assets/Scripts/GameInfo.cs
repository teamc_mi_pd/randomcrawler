using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "GameData")]
public class GameInfo : ScriptableObject
{
    public int NumberOfPlayer;
    public Pawn[] PlayerPawns;
    public GameObject PlayerPrefab;
    public Item[] ChestItem;
    public Item[] WeaponItem;

    public Pawn[] GenerateCharacter()
    {
        PlayerPawns = new Pawn[NumberOfPlayer];
        for (int i = 0; i < NumberOfPlayer; i++)
        {
            PlayerPawns[i] = Instantiate(PlayerPrefab).GetComponent<Pawn>();
        }
        return PlayerPawns;
    }
}
