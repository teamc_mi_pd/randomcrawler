using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameMode : MonoBehaviour
{
    private static GameMode Singleton;
    [SerializeField]
    GameObject EndGameMenu;
    [SerializeField]
    ToolTip Info;
    [SerializeField]
    private GameInfo m_gameData;
    [SerializeField]
    private GameObject Camera;
    [SerializeField]
    private GridManager m_gridManager;
    public static GameGrid Grid => Singleton.m_gridManager.GetGrid();
    private TurnManager m_turnManager;
    private PathFinder m_pathFinder;
    private GameModeProcessor m_processor;
    [SerializeField]
    private Vector2Int m_minRoomSize;
    [HideInInspector]
    public Pawn activePawm => m_turnManager.GetActivePawn();
    private List<Tile> AlteredTile = new List<Tile>();
    private List<Vector2Int> OpenEnd = new List<Vector2Int>();
    private Room CurrentRoom;


    [HideInInspector]
    public UnityEvent<Vector2Int> TileClicked = new UnityEvent<Vector2Int>();

    private bool bInMenu = false;

    private void Awake()
    {
        m_turnManager = new TurnManager();
        m_turnManager.NewTurn.AddListener(ShowReachableTile);
        m_pathFinder = new PathFinder(m_gridManager.GetGrid());
        m_processor = new GameModeProcessor(gameObject);
        Singleton ??= this;
    }
    private void Update()
    {
        m_processor.UpdateState(Time.deltaTime);
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            ToggleMenu();
        }
    }
    private void ToggleMenu()
    {
        bInMenu = !bInMenu;
        EndGameMenu.SetActive(bInMenu);
    }
    public static void OnTileCliked(Vector2Int t_ID)
    {
        Singleton.TileClicked?.Invoke(t_ID);

    }
    public static void OnTileRightCliked(Vector2Int t_ID)
    {
        Pawn pawn = Grid.GetTileByID(t_ID).GetPawn();
        if (pawn != null)
        {
            Singleton.Info.ShowTooltip(pawn,pawn.transform.position);
        }
    }

    public static void ResetGrid()
    {
        foreach (Tile t_tile in Singleton.AlteredTile)
        {
            t_tile.GetTileObject().GetComponent<MeshRenderer>().material = Resources.Load<Material>("Base");
            t_tile.Reachable = false;
        }
        Singleton.AlteredTile.Clear();
    }
    private void ShowReachableTile()
    {
        ResetGrid();
        Pawn ActivePawn = m_turnManager.GetActivePawn();
        List<Tile> EnemyPawns = new List<Tile>();
        Tile[] FollowPawns = m_pathFinder.GetReachableTile(Grid.GetTileByPawn(ActivePawn).ID, ActivePawn.TotalSpeed, EnemyPawns);
        foreach (Tile t_tile in FollowPawns)
        {
            AlteredTile.Add(t_tile);
            t_tile.GetTileObject().GetComponent<MeshRenderer>().material = Resources.Load<Material>("Selectable");
            t_tile.Reachable = true;
        }
        foreach (Tile t_tile in EnemyPawns)
        {
            AlteredTile.Add(t_tile);
            t_tile.GetTileObject().GetComponent<MeshRenderer>().material = Resources.Load<Material>("Pawn");
            t_tile.Reachable = true;
        }
        m_processor.SwitchState(m_processor.Idle);
    }
    public static Tile[] GetPathToPawn(Pawn t_target)
    {
        Tile[] output = null;
        int BaseLength = int.MaxValue;
        int i = 0;
        foreach (Tile tile in Grid.GetAdiacentNeighbors(Grid.GetTileByPawn(t_target).ID))
        {
            i++;
            Tile[] check = Singleton.m_pathFinder.TilePath(Grid.GetTileByPawn(Singleton.activePawm).ID, tile.ID, false);
            if (check != null && BaseLength > check.Length)
            {
                BaseLength = check.Length;
                output = check;
            }
        }
        return output;
    }
    public Tile[] PathToTile(Vector2Int t_start, Vector2Int t_end)
    {
        return m_pathFinder.TilePath(t_start, t_end);
    }
    public bool MovePawn(Pawn t_pawnToMove, Vector2Int t_target)
    {
        Tile[] Path = PathToTile(Grid.GetTileByPawn(t_pawnToMove).ID, t_target);
        if (Path != null)
        {
            if (Grid.MovePawn(t_pawnToMove, Path[Path.Length - 1].ID))
            {
                t_pawnToMove.StartFollowPath(Path);
            }
            if (OpenEnd.Contains(t_target))
            {
                CurrentRoom.Connection.ShowCorridor(Grid);
                OpenEnd.Clear();
                CurrentRoom = CurrentRoom.Connection.Data.EndRoom;
                foreach (Vector2Int ID in CurrentRoom.OnDiscover(Grid))
                {
                    Grid.GetTileByID(ID).HideTile(false);
                    OpenEnd.Add(ID);
                }
                List<EnemyPawn> pawns = new List<EnemyPawn>();
                foreach (Vector2Int ID in CurrentRoom.SpawnEnemy())
                {
                    pawns.Add(SpawnEnemy(ID));
                }
                m_turnManager.AddPawnToAI(pawns.ToArray());
            }
            return true;
        }
        return false;
    }
    private EnemyPawn SpawnEnemy(Vector2Int t_Location)
    {
        EnemyPawn newPawn = Instantiate(Resources.Load<GameObject>("EnemyMelee")).GetComponent<EnemyPawn>();
        Grid.AddNewPawn(newPawn, t_Location);
        newPawn.OnDeath.AddListener(PawnDeath);
        newPawn.transform.position = m_gridManager.GetTileLocation(t_Location);
        return newPawn;
    }
    private void SpawnPlayerPawn()
    {
        List<PlayerPawn> pawns = new List<PlayerPawn>();
        List<Vector2Int> ID = new List<Vector2Int>();
        int x;
        int y;
        bool bIsNew;
        foreach (Pawn newPawn in m_gameData.PlayerPawns)
        {
            bIsNew = true;
            while (bIsNew)
            {
                x = Random.Range(CurrentRoom.RoomBound.x, CurrentRoom.RoomBound.xMax);
                y = Random.Range(CurrentRoom.RoomBound.y, CurrentRoom.RoomBound.yMax);
                Vector2Int newID = new Vector2Int(x, y);
                if (!ID.Contains(newID))
                {
                    newPawn.OnDeath.AddListener(PawnDeath);
                    pawns.Add(newPawn.GetComponent<PlayerPawn>());
                    Grid.AddNewPawn(newPawn, newID);
                    newPawn.transform.position = m_gridManager.GetTileLocation(newID);
                    ID.Add(newID);
                    bIsNew = false;
                }
            }
        }
        m_turnManager.AddPawnToPlayers(pawns.ToArray());
    }

    private void PawnDeath(Pawn t_pawn)
    {
        Grid.RemovePawn(t_pawn);
        m_turnManager.PawnDeath(t_pawn);
    }

    public static void EndGame()
    {
        Singleton.m_processor.SwitchState(Singleton.m_processor.Moving);
        Singleton.EndGameMenu.SetActive(true);
    }
    public static void StartGame()
    {
        Singleton.GenerateDoungeon();
        Singleton.SpawnPlayerPawn();
        Singleton.ShowReachableTile();
        Singleton.Camera.transform.position = Singleton.CurrentRoom.GetCenter();
        Singleton.m_processor.SwitchState(Singleton.m_processor.Idle);

    }
    #region Procedural Generation
    private void GenerateDoungeon()
    {
        List<Room> RoomToConnect = new List<Room>();
        foreach (Room Room in BinarySpacePartitioning(new BoundsInt(new Vector3Int(0, 0, 0), new Vector3Int(Grid.GetSize().x, Grid.GetSize().y, 0)), m_minRoomSize.x, m_minRoomSize.y))
        {
            RoomToConnect.Add(Room);
            Room.FindExit();
        }
        Room StartingRoom = RoomToConnect[Random.Range(0, RoomToConnect.Count)];
        CurrentRoom = StartingRoom;
        while (RoomToConnect.Count > 1)
        {
            RoomToConnect.Remove(StartingRoom);
            CorridorData CorridorEnd = FindClosestRoom(StartingRoom, RoomToConnect);
            StartingRoom = ConnectRoom(CorridorEnd);
        }

        foreach (Vector2Int ID in CurrentRoom.OnDiscover(Grid))
        {
            Grid.GetTileByID(ID).HideTile(false);
            OpenEnd.Add(ID);
        }
    }
    private Room[] BinarySpacePartitioning(BoundsInt spaceToSplit, int minWidth, int minHeight)
    {
        Queue<BoundsInt> roomsQueue = new Queue<BoundsInt>();
        List<Room> roomsList = new List<Room>();
        roomsQueue.Enqueue(spaceToSplit);
        while (roomsQueue.Count > 0)
        {
            var room = roomsQueue.Dequeue();
            if (room.size.y >= minHeight && room.size.x >= minWidth)
            {
                if (Random.value < 0.5f)
                {
                    if (room.size.y >= minHeight * 2)
                    {
                        foreach (BoundsInt Area in SplitHorizontally(room))
                        {
                            roomsQueue.Enqueue(Area);
                        }
                    }
                    else if (room.size.x >= minWidth * 2)
                    {
                        foreach (BoundsInt Area in SplitVertically(room))
                        {
                            roomsQueue.Enqueue(Area);
                        }
                    }
                    else if (room.size.x >= minWidth && room.size.y >= minHeight)
                    {
                        roomsList.Add(new Room(room));
                    }
                }
                else
                {
                    if (room.size.x >= minWidth * 2)
                    {
                        foreach (BoundsInt Area in SplitVertically(room))
                        {
                            roomsQueue.Enqueue(Area);
                        }
                    }
                    else if (room.size.y >= minHeight * 2)
                    {
                        foreach (BoundsInt Area in SplitHorizontally(room))
                        {
                            roomsQueue.Enqueue(Area);
                        }
                    }
                    else if (room.size.x >= minWidth && room.size.y >= minHeight)
                    {

                        roomsList.Add(new Room(room));
                    }
                }
            }
        }
        return roomsList.ToArray();
    }
    private BoundsInt[] SplitVertically(BoundsInt room)
    {
        BoundsInt[] output = new BoundsInt[2];
        int xSplit = Random.Range(1, room.size.x);
        output[0] = new BoundsInt(room.min, new Vector3Int(xSplit, room.size.y, room.size.z));
        output[1] = new BoundsInt(new Vector3Int(room.min.x + xSplit, room.min.y, room.min.z),
            new Vector3Int(room.size.x - xSplit, room.size.y, room.size.z));
        return output;
    }
    private BoundsInt[] SplitHorizontally(BoundsInt room)
    {
        BoundsInt[] output = new BoundsInt[2];
        var ySplit = Random.Range(1, room.size.y);
        output[0] = new BoundsInt(room.min, new Vector3Int(room.size.x, ySplit, room.size.z));
        output[1] = new BoundsInt(new Vector3Int(room.min.x, room.min.y + ySplit, room.min.z),
            new Vector3Int(room.size.x, room.size.y - ySplit, room.size.z));
        return output;
    }
    private Room ConnectRoom(CorridorData t_data)
    {
        Corridor newCorridor = new Corridor(t_data);
        newCorridor.CalculatePath();
        t_data.StartRoom.Connection = newCorridor;
        return t_data.EndRoom;
    }
    private CorridorData FindClosestRoom(Room startingRoom, List<Room> t_RoomToConnect)
    {
        CorridorData Info = new CorridorData();
        Info.StartRoom = startingRoom;
        float distance = float.MaxValue;
        for (int i = 0; i < 4; i++)
        {
            Vector2Int startPoint = startingRoom.PossibleExit[i * 2];
            foreach (Room room in t_RoomToConnect)
            {
                for (int j = 0; j < 4; j++)
                {
                    Vector2Int endPoint = room.PossibleExit[j * 2];
                    float CurrentDistance = Vector2.Distance(endPoint, startPoint);
                    if (CurrentDistance < distance)
                    {
                        Info.Entrance = i * 2;
                        Info.Exit = j * 2;
                        Info.EndRoom = room;
                        Info.StartPoint = startPoint;
                        Info.EndPoint = endPoint;
                        distance = CurrentDistance;
                    }
                }
            }
        }
        return Info;
    }

    #endregion

}
