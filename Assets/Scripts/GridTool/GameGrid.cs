using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;

[System.Serializable]
public class GameGrid
{
    [SerializeField]
    private Vector2Int m_size;
    private Tile[,] m_tiles;
    //needed for serialization 
    [SerializeField]
    private List<Tile> m_tilesList;
    private Dictionary<Pawn, Tile> m_pawnsPosition;
    public GameGrid(Vector2Int t_size, Vector2 t_tileSize, Vector2 t_tileOffset)
    {
        m_size = t_size;
    }
    public Vector2Int GetSize() => m_size;
    public Tile[] EditorCreateGrid()
    {
        m_tilesList = new List<Tile>();
        for (int i = 0; i < m_size.x; i++)
        {
            for (int j = 0; j < m_size.y; j++)
            {
                m_tilesList.Add(new Tile(new Vector2Int(i, j)));
            }
        }
        return m_tilesList.ToArray();
    }
    public void SaveMatrix()
    {
        m_pawnsPosition = new Dictionary<Pawn, Tile>();
        m_tiles = new Tile[m_size.x, m_size.y];
        foreach (Tile t_tile in m_tilesList)
        {
            m_tiles[t_tile.ID.x, t_tile.ID.y] = t_tile;
        }
    }
    public Tile GetTileByID(Vector2Int t_ID)
    {
        if (t_ID.x < 0 || t_ID.y < 0 || t_ID.x > m_size.x - 1 || t_ID.y > m_size.y - 1)
            return null;
        return m_tiles[t_ID.x, t_ID.y];
    }
    public Tile GetTileByPawn(Pawn t_pawn)
    {
        if (m_pawnsPosition.TryGetValue(t_pawn, out Tile output))
            return output;
        else
            return null;
    }
    public bool MovePawn(Pawn t_pawn, Vector2Int t_tileID)
    {
        Tile EndingTile = GetTileByID(t_tileID);
        Tile StartingTile = GetTileByPawn(t_pawn);
        if (m_pawnsPosition.TryGetValue(t_pawn, out Tile Value) && StartingTile != null && EndingTile != null)
        {
            EndingTile.SetPawn(t_pawn);
            StartingTile.RemovePawn();
            m_pawnsPosition[t_pawn] = EndingTile;
            return true;
        }
        else
        {
            return false;
        }
    }
    public bool AddNewPawn(Pawn t_pawn, Vector2Int t_tile)
    {
        Tile EndingTile = GetTileByID(t_tile);
        if (EndingTile.GetPawn() == null)
        {
            EndingTile.SetPawn(t_pawn);
            m_pawnsPosition.Add(t_pawn, EndingTile);
            t_pawn.gameObject.transform.position = EndingTile.GetTileObject().transform.position;
            return true;
        }
        return false;
    }
    public bool RemovePawn(Pawn t_pawn)
    {
        if(t_pawn != null)
        {
            m_pawnsPosition[t_pawn].RemovePawn();
            return m_pawnsPosition.Remove(t_pawn);
        }
        return false;
    }
    public Tile[,] GetAllTile()
    {
        return m_tiles;
    }
    public Tile[] GetAdiacentNeighbors(Vector2Int t_tileID)
    {
        List<Tile> Output = new List<Tile>();
        for (int i = 0; i < 4; i++)
        {
            Tile Check = GetTileByID(t_tileID + Utils.FullDirection[i]);
            if (Check != null)
            {
                Output.Add(Check);
            }
        }
        return Output.ToArray();
    }
    public Tile[] GetNeighbors(Vector2Int t_tileID)
    {
        List<Tile> Output = new List<Tile>();
        for (int i = 0; i < Utils.FullDirection.Length; i++)
        {
            Tile Check = GetTileByID(t_tileID + Utils.FullDirection[i]);
            if (Check != null)
            {
                Output.Add(Check);
            }
        }
        return Output.ToArray();
    }
}

public static class Utils
{
    public static Vector2Int[] FullDirection =
    {
        new Vector2Int(1,0),
        new Vector2Int(-1,0),
        new Vector2Int(0,1),
        new Vector2Int(0,-1),
        new Vector2Int(1,1),
        new Vector2Int(1,-1),
        new Vector2Int(-1,1),
        new Vector2Int(-1,-1)
    };
}
