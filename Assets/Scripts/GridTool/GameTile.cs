using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

public class GameTile : MonoBehaviour
{
    [SerializeField]
    public UnityEvent TileClicked = new UnityEvent();
    [SerializeField]
    public UnityEvent TileRightClicked = new UnityEvent();
    private void OnMouseDown()
    {
        TileClicked?.Invoke();
    }
    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(1))
        {
            TileRightClicked?.Invoke();
        }
    }

}
