using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    [SerializeField]
    private GridInfo m_gridData;
    [SerializeField, HideInInspector]
    private List<GameObject> m_tileList;
    [SerializeField, HideInInspector]
    private GameGrid m_grid;

    public void ClearList()
    {
        m_tileList.Clear();
    }
    public void EditorCreateGrid()
    {
        if (m_tileList != null)
        {
            foreach (GameObject s in m_tileList)
            {
                DestroyImmediate(s);
            }
            m_tileList.Clear();
        }
        else
        {
            m_tileList = new List<GameObject>();
        }
        m_grid = new GameGrid(m_gridData.Size, m_gridData.TileSize, m_gridData.Offset);
        foreach (Tile x in m_grid.EditorCreateGrid())
        {
            GameObject NewTile = Instantiate(m_gridData.TilePrefab, GetTileLocation(x.ID), Quaternion.identity);
            m_tileList.Add(NewTile);
            x.SetTileObject(NewTile);
            NewTile.transform.SetParent(transform);
        }
    }
    public GameGrid GetGrid() => m_grid;
    public GridInfo GetGridInfo() => m_gridData;
    public Vector3 GetTileLocation(Vector2Int t_ID)
    {
        return new Vector3(transform.position.x + t_ID.x * (m_gridData.TileSize.x + m_gridData.Offset.x), 0, transform.position.z + t_ID.y * (m_gridData.TileSize.y + m_gridData.Offset.y));
    }
    public bool AddPawnToGrid(Pawn t_pawn, Vector2Int t_tileID)
    {
        return m_grid.AddNewPawn(t_pawn.GetComponent<PlayerPawn>(), t_tileID);
    }
    public bool MovePawn(Pawn t_pawn, Vector2Int t_location)
    {
       return m_grid.MovePawn(t_pawn, t_location);
    }
    private void Awake()
    {
        m_grid.SaveMatrix();
        foreach (Tile t_tile in m_grid.GetAllTile())
        {
            t_tile.SetUpTile();
            t_tile.HideTile(true);
        }

    }
}
