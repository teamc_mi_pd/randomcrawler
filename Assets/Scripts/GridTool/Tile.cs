using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class Tile
{
    public bool bIsPlayable = true;
    private Pawn m_activePawn;
    [SerializeField]
    private GameObject m_tileObject;
    [SerializeField]
    public Vector2Int ID;
    public Tile ConnectedTile;
    public bool Reachable;


    //A* var
    public int Cost;
    public int Distance;
    public int CostDistance;
    public Tile Parent;
    public void SetDistance(Vector2Int t_Target)
    {
        Distance = Mathf.Abs(t_Target.x - ID.x) + Mathf.Abs(t_Target.y - ID.y);
    }
    public void ResetTile()
    {
        Cost = int.MaxValue;
        Distance = 0;
        SetCostDistance();
        Parent = null;
    }
    public void SetCostDistance()
    {
        CostDistance = Cost + Distance;
    }
    public Tile(Vector2Int t_ID)
    {
        ID = t_ID;
    }
    public void SetTileObject(GameObject t_tile)
    {
        m_tileObject = t_tile;
    }
    void TileCliked()
    {
        if (Reachable)
            GameMode.OnTileCliked(ID);
    }
    public void SetUpTile()
    {
        m_tileObject.GetComponent<GameTile>().TileClicked.AddListener(TileCliked);
        m_tileObject.GetComponent<GameTile>().TileRightClicked.AddListener(RightClick);
    }
    private void RightClick()
    {
        GameMode.OnTileRightCliked(ID);
    }
    public GameObject GetTileObject() => m_tileObject;
    public void RemovePawn()
    {
        m_activePawn = null;
    }
    public void SetPawn(Pawn t_pawn)
    {
        m_activePawn = t_pawn;
    }
    public Pawn GetPawn() => m_activePawn;
    public void HideTile(bool bHidden)
    {
        m_tileObject.SetActive(!bHidden);
        bIsPlayable = !bHidden;
    }
}
