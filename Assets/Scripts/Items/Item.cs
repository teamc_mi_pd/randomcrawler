using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Item/ItemData")]
public class Item : ScriptableObject
{
    public Sprite Icon;

    public int Armor;
    public int Health;
    public int Speed;
    public int Damage;
}
