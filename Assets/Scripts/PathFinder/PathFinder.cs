using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
public class PathFinder
{
    /* //1st Try from old project, Bugged in checking VisitedNode List (create new node each time)
    public Vector2Int[] NodePathFinder(Vector2Int start, Vector2Int finish, GameGrid t_grid)
    {
        PathNode startingNode = new PathNode(start);
        startingNode.SetDistance(finish);
        List<PathNode> activeNodes = new List<PathNode>
        {
            startingNode
        };
        List<PathNode> visitedNodes = new List<PathNode>();
        while (activeNodes.Count > 0)
        {
            PathNode checkNode = activeNodes.OrderByDescending(x => x.CostDistance).Last();
            if (checkNode.ID == finish)
            {
                activeNodes.Clear();
                PathNode node = checkNode;
                List<Vector2Int> Path = new List<Vector2Int>();
                while (true)
                {
                    Path.Add(node.ID);
                    node = node.Parent;
                    if (node == null)
                    {
                        return Path.ToArray();
                    }
                }
            }
            visitedNodes.Add(checkNode);
            activeNodes.Remove(checkNode);
            List<PathNode> walkableNodes = new List<PathNode>();

            walkableNodes = GetWalkableNodes(t_grid, checkNode, finish, start);

            foreach (PathNode walkableNode in walkableNodes)
            {
                if (visitedNodes.Contains(walkableNode))
                {
                    Debug.Log("works");
                    continue;
                }
                if (activeNodes.Contains(walkableNode))
                {
                    PathNode existingTile = activeNodes.First(Node => Node.ID == walkableNode.ID);
                    if (existingTile.CostDistance > checkNode.CostDistance)
                    {
                        activeNodes.Remove(existingTile);
                        activeNodes.Add(walkableNode);
                    }
                }
                else
                {
                    activeNodes.Add(walkableNode);
                }
            }
        }
        return new Vector2Int[0];
    }
    private List<PathNode> GetWalkableNodes(GameGrid map, PathNode currentNode, Vector2Int targetTile, Vector2Int startTile)
    {
        List<PathNode> possibleNodes = new List<PathNode>(){
        new PathNode(new Vector2Int(currentNode.ID.x + 1, currentNode.ID.y), currentNode.Cost + 1, currentNode),
        new PathNode(new Vector2Int(currentNode.ID.x - 1, currentNode.ID.y), currentNode.Cost + 1, currentNode),
        new PathNode(new Vector2Int(currentNode.ID.x, currentNode.ID.y + 1), currentNode.Cost + 1, currentNode),
        new PathNode(new Vector2Int(currentNode.ID.x, currentNode.ID.y - 1), currentNode.Cost + 1, currentNode),
        };
        List<PathNode> Output = new List<PathNode>();
        foreach (PathNode node in possibleNodes)
        {
            Tile Tileref = map.GetTileByID(new Vector2Int(node.ID.x, node.ID.y));
            bool Walkable = Tileref != null && Tileref.bIsPlayable && (Tileref.GetPawn() == null || Tileref.ID == startTile);
            if (Walkable)
            {
                node.SetDistance(targetTile);
                Output.Add(node);
            }
        }
        return Output;
    }
    */

    private readonly GameGrid m_grid;
    public PathFinder(GameGrid t_grid)
    {
        m_grid = t_grid;
    }

    // Visual movimento in diagonale 
    public Tile[] TilePath(Vector2Int t_start, Vector2Int t_finish, bool t_reachable = true)
    {
        foreach (Tile t_tile in m_grid.GetAllTile())
        {
            t_tile.ResetTile();
        }
        Tile startingTile = m_grid.GetTileByID(t_start);
        List<Tile> activeTiles = new List<Tile>
        {
            startingTile
        };
        List<Tile> visitedTiles = new List<Tile>();
        startingTile.Cost = 0;
        startingTile.SetDistance(t_finish);
        startingTile.SetCostDistance();
        while (activeTiles.Count > 0)
        {
            Tile checkTile = GetLowestCostDistanceTile(activeTiles);
            if (checkTile.ID == t_finish)
            {
                activeTiles.Clear();
                visitedTiles.Clear();
                Tile node = checkTile;
                List<Tile> Path = new List<Tile>();
                while (true)
                {
                    Path.Add(node);
                    node = node.Parent;
                    if (node == null)
                    {
                        List<Tile> output = new List<Tile>();
                        for (int i = Path.Count - 1; i >= 0; i--)
                        {
                            output.Add(Path[i]);
                        }
                        return output.ToArray();
                    }
                }
            }
            visitedTiles.Add(checkTile);
            activeTiles.Remove(checkTile);
            foreach (Tile t_tile in InitNeighbors(m_grid.GetNeighbors(checkTile.ID), t_reachable))
            {
                if (visitedTiles.Contains(t_tile))
                {
                    continue;
                }
                int pathCost;
                Vector2Int Distance = checkTile.ID - t_tile.ID;
                if (Mathf.Abs(Distance.x) != Mathf.Abs(Distance.y))
                    pathCost = checkTile.Cost + 10;
                else
                    pathCost = checkTile.Cost + 14;
                if (pathCost < t_tile.Cost)
                {
                    t_tile.Parent = checkTile;
                    t_tile.Cost = pathCost;
                    t_tile.SetDistance(t_finish);
                    t_tile.SetCostDistance();
                }
                if (!activeTiles.Contains(t_tile))
                {
                    activeTiles.Add(t_tile);
                }
            }
        }
        return null;
    }
    private Tile GetLowestCostDistanceTile(List<Tile> t_tiles)
    {
        Tile lowestCostDistanceTile = t_tiles[0];
        for (int i = 1; i < t_tiles.Count; i++)
        {
            if (t_tiles[i].CostDistance < lowestCostDistanceTile.CostDistance)
            {
                lowestCostDistanceTile = t_tiles[i];
            }
        }
        return lowestCostDistanceTile;
    }

    //generate path only in reachable tiles with no diagonal movement
    private Tile[] InitNeighbors(Tile[] t_neighbors, bool t_reachable = true)
    {
        List<Tile> output = new List<Tile>();
        foreach (Tile neighbor in t_neighbors)
        {
            if (neighbor.bIsPlayable && neighbor.GetPawn() == null && (t_reachable ? neighbor.Reachable : true))
            {
                output.Add(neighbor);
            }
        }
        return output.ToArray();
    }
    private Tile[] InitNeighbors(Tile[] t_neighbors, List<Tile> EnemyTile, bool t_reachable = true)
    {
        List<Tile> output = new List<Tile>();
        foreach (Tile neighbor in t_neighbors)
        {
            if (neighbor.bIsPlayable && neighbor.GetPawn() == null && (t_reachable ? neighbor.Reachable : true))
            {
                output.Add(neighbor);
            }
            else if (neighbor.GetPawn() != null && neighbor.GetPawn().GetComponent<PlayerPawn>() == null)
            {
                EnemyTile.Add(neighbor);
            }
        }
        return output.ToArray();
    }
    public Tile[] GetReachableTile(Vector2Int t_start, int t_range, List<Tile> EnemyTile)
    {
        Tile startingTile = m_grid.GetTileByID(t_start);
        Queue<Tile> activeTiles = new Queue<Tile>();
        activeTiles.Enqueue(startingTile);
        List<Tile> visitedTiles = new List<Tile>();
        foreach (Tile t_tile in m_grid.GetAllTile())
        {
            t_tile.ResetTile();
        }
        startingTile.Cost = 0;
        while (activeTiles.Count > 0)
        {
            Tile checkTile = activeTiles.Dequeue();
            visitedTiles.Add(checkTile);
            Tile[] NeibghorsTiles = InitNeighbors(m_grid.GetAdiacentNeighbors(checkTile.ID), EnemyTile, false);
            foreach (Tile t_tile in NeibghorsTiles)
            {
                if (visitedTiles.Contains(t_tile))
                {
                    continue;
                }
                int pathCost = checkTile.Cost + 1;
                t_tile.Cost = pathCost;
                if (!activeTiles.Contains(t_tile) && pathCost <= t_range)
                {
                    activeTiles.Enqueue(t_tile);
                }
            }
        }
        visitedTiles.Remove(startingTile);
        return visitedTiles.ToArray();
    }

}
