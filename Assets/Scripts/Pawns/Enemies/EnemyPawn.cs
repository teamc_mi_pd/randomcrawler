using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPawn : Pawn
{
    [SerializeField]
    protected EnemyStats m_data;
    protected PlayerPawn[] m_playerPawns;
    public PlayerPawn[] PlayersRef => m_playerPawns;
    public override void Awake()
    {
        base.Awake();
        m_processor = new AIProcessor(gameObject);
        m_processor.SwitchState(m_processor.Wait);
    }
    public void SetPlayerRef(PlayerPawn[] t_playerPawns)
    {
        m_playerPawns = t_playerPawns;
    }
    public void AwakePawn()
    {
        m_processor.SwitchState(((AIProcessor)m_processor).Awake);
    }
}
