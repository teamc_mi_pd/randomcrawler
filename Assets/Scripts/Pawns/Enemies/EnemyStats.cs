using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy/EnemyData")]
public class EnemyStats : ScriptableObject
{
    [Tooltip("NumberOfTileForTurn")]
    public int MoveSpeed;
    [Tooltip("TimeToMove1Tile")]
    public float Speed;
    [Tooltip("TurnOrder")]
    public float TurnSpeed;
    [Tooltip("RangeOfAttack(tile)")]
    public float AttackRange;

}
