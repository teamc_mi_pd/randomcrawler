using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Pawn : MonoBehaviour
{
    [HideInInspector]
    public int TurnSpeed;
    [Tooltip("How many tile the pawn can move")]
    public int AreaOfMovement;
    [Tooltip("Time to move 1 tile (s)")]
    public float MovementSpeed;
    [HideInInspector]
    public bool bIsPlayer;
    private Tile[] m_path;
    [HideInInspector]
    public Pawn Target;
    public Tile[] Path => m_path;
    [HideInInspector]
    public PawnAction ActionType;
    [HideInInspector]
    public int Index = 0;
    [SerializeField]
    protected PawnProcessor m_processor;
    [HideInInspector]
    public UnityEvent OnTurnCompletead = new UnityEvent();
    [HideInInspector]
    public UnityEvent<Pawn> OnDeath = new UnityEvent<Pawn>();
    [SerializeField]
    private int m_maxHealth;
    [SerializeField]
    private float m_damage;
    [SerializeField]
    private int m_armor;
    [HideInInspector]
    public float Health;
    public int TotalSpeed => AreaOfMovement + MainHand.Speed + Chest.Speed;
    public float Damage => m_damage + MainHand.Damage + Chest.Damage;
    public float MaxHealth => m_maxHealth + MainHand.Health + Chest.Health;
    public float Armor => m_armor + MainHand.Armor + Chest.Armor;


    [HideInInspector]
    public Mesh PawnMesh;
    [HideInInspector]
    public Item MainHand;
    [HideInInspector]
    public Item Chest;

    public void TakeDamage(float t_damage)
    {
        Health -= t_damage - Armor;
        if (Health <= 0)
        {
            Death();
        }
    }
    private void Death()
    {
        OnDeath?.Invoke(this);
        gameObject.SetActive(false);
    }
    public void StartFollowPath(Tile[] t_path)
    {
        if (t_path != null)
        {
            m_path = t_path;
            Index = 0;
            m_processor.SwitchState(m_processor.Move);
        }
    }
    private void Update()
    {
        m_processor.UpdateState(Time.deltaTime);
    }
    public virtual void Awake()
    {
        //print(this);
        MainHand = ScriptableObject.CreateInstance<Item>();
        Chest = ScriptableObject.CreateInstance<Item>();
        Health = MaxHealth;

    }
}

public enum PawnAction
{
    None = 0,
    Moving = 1,
    Attacking = 2
}

