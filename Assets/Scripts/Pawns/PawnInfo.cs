using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct PawnInfo 
{
    public Item MainHand;
    public Item Chest;
}
