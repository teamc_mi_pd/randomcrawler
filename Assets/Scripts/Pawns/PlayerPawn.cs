using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPawn : Pawn
{
    public override void Awake()
    {
        base.Awake();
        m_processor = new PawnProcessor(gameObject);
        m_processor.SwitchState(m_processor.Wait);
    }

}
