using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Corridor
{
    public Dictionary<Vector2Int, Room> Connections;
    public List<Vector2Int> Path = new List<Vector2Int>();
    public CorridorData Data;

    public Corridor(CorridorData t_data)
    {
        Data = t_data;
    }
    public void ShowCorridor(GameGrid t_grid)
    {
        foreach (Vector2Int ID in Path)
        {
            t_grid.GetTileByID(ID).HideTile(false);
        }
        t_grid.GetTileByID(Data.EndRoom.PossibleExit[Data.Exit]).HideTile(false);
        t_grid.GetTileByID(Data.EndRoom.PossibleExit[Data.Exit + 1]).HideTile(false);
    }
    public void CalculatePath()
    {
        Vector2Int Distance = Data.EndPoint - Data.StartPoint;
        Vector2Int nextPoint = Data.StartPoint;
        if (Mathf.Abs(Distance.x) > Mathf.Abs(Distance.y))
        {
            while (Distance.x != 0)
            {
                nextPoint = nextPoint + new Vector2Int(1 * (int)Mathf.Sign(Distance.x), 0);
                Path.Add(nextPoint);
                Distance = Data.EndPoint - nextPoint;
            }
            while (Distance.y != 0)
            {
                nextPoint = nextPoint + new Vector2Int(0, (int)Mathf.Sign(Distance.y));
                Path.Add(nextPoint);
                Distance = Data.EndPoint - nextPoint;
            }
        }
        else
        {
            while (Distance.y != 0)
            {
                nextPoint = nextPoint + new Vector2Int(0, (int)Mathf.Sign(Distance.y));
                Path.Add(nextPoint);
                Distance = Data.EndPoint - nextPoint;
            }
            while (Distance.x != 0)
            {
                nextPoint = nextPoint + new Vector2Int(1 * (int)Mathf.Sign(Distance.x), 0);
                Path.Add(nextPoint);
                Distance = Data.EndPoint - nextPoint;
            }
        }
        Path.Remove(Data.EndPoint);
    }
}
public struct CorridorData
{
    public Vector2Int StartPoint;
    public Vector2Int EndPoint;
    public Room EndRoom;
    public Room StartRoom;
    public int Exit;
    public int Entrance;

}
