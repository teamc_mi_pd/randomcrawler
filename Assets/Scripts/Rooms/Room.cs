using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room
{
    public BoundsInt RoomBound;
    public Vector2Int Center;
    public Vector2Int[] PossibleExit;
    public Corridor Connection;

    public Room(BoundsInt roomBound)
    {
        RoomBound = roomBound;
        RoomBound.y = roomBound.y + 2;
        RoomBound.x = roomBound.x + 2;
        RoomBound.xMax = roomBound.xMax - 2;
        RoomBound.yMax = roomBound.yMax - 2;
    }

    public Vector3 GetCenter()
    {
        return new Vector3(RoomBound.center.x, RoomBound.center.z, RoomBound.center.y) + new Vector3(0, 16, -6);
    }
    public virtual Vector2Int[] OnDiscover(GameGrid t_grid)
    {
        List<Vector2Int> output = new List<Vector2Int>();
        ShowRoom(t_grid);
        if (Connection != null)
        {
            output.Add(PossibleExit[Connection.Data.Entrance]);
            output.Add(PossibleExit[Connection.Data.Entrance + 1]);
        }
        return output.ToArray();
    }

    private void ShowRoom(GameGrid t_grid)
    {
        for (int i = RoomBound.x; i < RoomBound.xMax; i++)
        {
            for (int j = RoomBound.y; j < RoomBound.yMax; j++)
            {
                t_grid.GetTileByID(new Vector2Int(i, j)).HideTile(false);
            }
        }
    }

    public Vector2Int FindExit()
    {
        Center = new Vector2Int(Mathf.CeilToInt(RoomBound.center.x), Mathf.CeilToInt(RoomBound.center.y));
        PossibleExit = new Vector2Int[8];
        PossibleExit[0] = new Vector2Int(RoomBound.x - 2, Vector2Int.RoundToInt(RoomBound.center).y);
        PossibleExit[1] = new Vector2Int(RoomBound.x - 1, Vector2Int.RoundToInt(RoomBound.center).y);
        PossibleExit[2] = new Vector2Int(RoomBound.xMax + 1, Vector2Int.RoundToInt(RoomBound.center).y);
        PossibleExit[3] = new Vector2Int(RoomBound.xMax, Vector2Int.RoundToInt(RoomBound.center).y);
        PossibleExit[4] = new Vector2Int(Vector2Int.RoundToInt(RoomBound.center).x, RoomBound.y - 2);
        PossibleExit[5] = new Vector2Int(Vector2Int.RoundToInt(RoomBound.center).x, RoomBound.y - 1);
        PossibleExit[6] = new Vector2Int(Vector2Int.RoundToInt(RoomBound.center).x, RoomBound.yMax + 1);
        PossibleExit[7] = new Vector2Int(Vector2Int.RoundToInt(RoomBound.center).x, RoomBound.yMax);
        return Center;
    }

    public Vector2Int[] SpawnEnemy()
    {
        //int MaxIndex = 1;
        int MaxIndex = Mathf.FloorToInt((RoomBound.xMax - RoomBound.x) * (RoomBound.yMax - RoomBound.y) / 64);
        List<Vector2Int> output = new List<Vector2Int>();
        int x;
        int y;
        for (int i = 0; i < MaxIndex; i++)
        {
            bool bIsNew = true;
            while (bIsNew)
            {
                x = UnityEngine.Random.Range(RoomBound.x, RoomBound.xMax);
                y = UnityEngine.Random.Range(RoomBound.y, RoomBound.yMax);
                Vector2Int newID = new Vector2Int(x, y);
                if (!output.Contains(newID))
                {
                    output.Add(newID);
                    bIsNew = false;
                }
            }
        }
        return output.ToArray();
    }
}
