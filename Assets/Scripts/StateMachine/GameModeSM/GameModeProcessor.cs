using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

public class GameModeProcessor : Processor
{
    public PlayerMovingState Moving;
    public IdleState Idle;
    protected GameMode m_gameModeOwner;
    public GameMode gameMode => m_gameModeOwner;
    public GameModeProcessor(GameObject t_owner) : base(t_owner)
    {
        m_gameModeOwner = t_owner.GetComponent<GameMode>();
        Moving = new PlayerMovingState(this);
        Idle = new IdleState(this);
    }
}
