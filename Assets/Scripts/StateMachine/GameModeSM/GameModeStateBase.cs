using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameModeStateBase : StateBase
{
    private GameModeProcessor m_processorGame;
    public GameModeProcessor processorGame => m_processorGame;
    protected GameModeStateBase(Processor processor) : base(processor)
    {
        m_processorGame = processor as GameModeProcessor;
    }
}
