using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class IdleState : GameModeStateBase
{
    public IdleState(Processor processor) : base(processor)
    {
    }

    public override void OnStateEnter()
    {
        processorGame.gameMode.TileClicked.AddListener(SelectPlayerAction);
    }

    public override void OnStateExit()
    {
        processorGame.gameMode.TileClicked.RemoveListener(SelectPlayerAction);
    }

    public override void StateTick(float deltaTime)
    {


    }

    private void SelectPlayerAction(Vector2Int t_Tile)
    {
        if (GameMode.Grid.GetTileByID(t_Tile).GetPawn() != null)
        {
            processorGame.gameMode.activePawm.ActionType = PawnAction.Attacking;
            processorGame.gameMode.activePawm.Target = GameMode.Grid.GetTileByID(t_Tile).GetPawn();
            Tile[] Path = GameMode.GetPathToPawn(GameMode.Grid.GetTileByID(t_Tile).GetPawn());
            if (Path.Length > 1)
            {
                int PositionIndex = processorGame.gameMode.activePawm.AreaOfMovement > Path.Length - 1 ? Path.Length - 1 : processorGame.gameMode.activePawm.AreaOfMovement;
                GameMode.Grid.MovePawn(processorGame.gameMode.activePawm, Path[PositionIndex].ID);
            }
            processorGame.SwitchState(processorGame.Moving);
            processorGame.gameMode.activePawm.StartFollowPath(Path);
        }
        else
        {
            processorGame.gameMode.activePawm.ActionType = PawnAction.Moving;
            processorGame.gameMode.MovePawn(processorGame.gameMode.activePawm, t_Tile);
            processorGame.SwitchState(processorGame.Moving);
        }
    }
}
