using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPawnState : PawnStateBase
{
    public AttackPawnState(Processor processor) : base(processor)
    {
    }

    public override void OnStateEnter()
    {
        Vector2Int OwnerTile = GameMode.Grid.GetTileByPawn(m_pawnProcessor.pawnOwner).ID;
        Vector2Int TargetTile = GameMode.Grid.GetTileByPawn(m_pawnProcessor.pawnOwner.Target).ID;
        if (m_pawnProcessor.pawnOwner.Index < m_pawnProcessor.pawnOwner.AreaOfMovement && Vector2Int.Distance(TargetTile, OwnerTile) <= 1.6)
        {
            m_pawnProcessor.pawnOwner.Target.TakeDamage(m_pawnProcessor.pawnOwner.Damage);
        }
        m_pawnProcessor.pawnOwner.OnTurnCompletead?.Invoke();
        m_pawnProcessor.SwitchState(m_pawnProcessor.Wait);
    }

    public override void OnStateExit()
    {
        //throw new System.NotImplementedException();
    }

    public override void StateTick(float deltaTime)
    {
        //throw new System.NotImplementedException();
    }
}
