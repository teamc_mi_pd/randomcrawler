using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIProcessor : PawnProcessor
{
    private EnemyPawn m_AIOwner;
    public EnemyPawn AIOwner => m_AIOwner;
    public AwakeIAState Awake;
    public AIProcessor(GameObject t_owner) : base(t_owner)
    {
        m_AIOwner = t_owner.GetComponent<EnemyPawn>(); 
        Awake = new AwakeIAState(this);
    }

}
