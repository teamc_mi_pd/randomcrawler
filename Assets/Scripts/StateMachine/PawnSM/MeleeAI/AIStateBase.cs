using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIStateBase : PawnStateBase
{
    protected AIProcessor m_AIProcessor;
    public AIProcessor processor => m_AIProcessor;
    public AIStateBase(Processor processor) : base(processor)
    {
        m_AIProcessor = processor as AIProcessor;
    }

    public override void OnStateEnter()
    {
        throw new System.NotImplementedException();
    }

    public override void OnStateExit()
    {
        throw new System.NotImplementedException();
    }

    public override void StateTick(float deltaTime)
    {
        throw new System.NotImplementedException();
    }
}
