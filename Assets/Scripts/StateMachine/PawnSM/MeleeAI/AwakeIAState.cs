using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AwakeIAState : AIStateBase
{
    public AwakeIAState(Processor processor) : base(processor)
    {
    }

    public override void OnStateEnter()
    {
        FindClosest(m_AIProcessor.AIOwner.PlayersRef);
    }

    public override void OnStateExit()
    {
        //throw new System.NotImplementedException();
    }

    public override void StateTick(float deltaTime)
    {
        // throw new System.NotImplementedException();
    }

    private Pawn FindClosest(Pawn[] t_pawns)
    {
        Pawn Output = null;
        int Length = int.MaxValue;
        Tile[] Path = null;
        foreach (Pawn pawn in t_pawns)
        {
            Tile[] CheckPath = GameMode.GetPathToPawn(pawn);
            if (CheckPath == null)
            {
                continue;
            }
            int CheckLength = CheckPath.Length;
            if (CheckLength < Length)
            {
                Length = CheckLength;
                Output = pawn;
                Path = CheckPath;
            }
        }
        m_AIProcessor.AIOwner.Target = Output;
        if (Path != null)
        {
            m_AIProcessor.AIOwner.ActionType = PawnAction.Attacking;
            int PositionIndex = m_AIProcessor.AIOwner.AreaOfMovement > Path.Length - 1 ? Path.Length - 1 : m_AIProcessor.AIOwner.AreaOfMovement;
            GameMode.Grid.MovePawn(m_AIProcessor.AIOwner, Path[PositionIndex].ID);
            m_AIProcessor.AIOwner.StartFollowPath(Path);
        }
        else
        {
            m_pawnProcessor.pawnOwner.OnTurnCompletead?.Invoke();
        }

        return Output;
    }
}
