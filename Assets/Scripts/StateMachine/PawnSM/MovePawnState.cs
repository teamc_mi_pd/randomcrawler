using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePawnState : PawnStateBase
{
    private float m_alpha = 0;
    private Vector3 Start = Vector3.zero;
    private Vector3 End = Vector3.zero;

    public MovePawnState(Processor processor) : base(processor)
    {

    }

    public override void OnStateEnter()
    {
        Pawn User = m_pawnProcessor.pawnOwner;
        Start = User.Path[User.Index].GetTileObject().transform.position;
        if (User.Index + 1 > User.Path.Length - 1 || User.Index + 1 > User.TotalSpeed)
        {
            if (User.ActionType == PawnAction.Moving)
            {
                User.OnTurnCompletead?.Invoke();
                m_pawnProcessor.SwitchState(m_pawnProcessor.Wait);
            }
            else if(User.ActionType == PawnAction.Attacking)
            {
                m_pawnProcessor.SwitchState(m_pawnProcessor.Attack);
            }
        }
        else
        {
            User.Index++;
            End = User.Path[User.Index].GetTileObject().transform.position;
        }
    }

    public override void OnStateExit()
    {
        m_alpha = 0;
        Start = Vector3.zero;
        End = Vector3.zero;
    }

    public override void StateTick(float deltaTime)
    {
        m_alpha += Time.deltaTime;
        pawnProcessor.pawnOwner.transform.position = Vector3.Lerp(Start, End, m_alpha / pawnProcessor.pawnOwner.MovementSpeed);
        if (m_alpha / pawnProcessor.pawnOwner.MovementSpeed >= 1)
        {
            pawnProcessor.SwitchState(pawnProcessor.Move);
        }
    }
}
