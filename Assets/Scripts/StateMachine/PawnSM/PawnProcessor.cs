using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PawnProcessor : Processor
{
    protected Pawn m_pawnOwner;
    public Pawn pawnOwner => m_pawnOwner;
    public WaitPawnState Wait;
    public MovePawnState Move;
    public AttackPawnState Attack;
    public PawnProcessor(GameObject t_owner) : base(t_owner)
    {
        m_pawnOwner = t_owner.GetComponent<Pawn>();
        Wait = new WaitPawnState(this);
        Move = new MovePawnState(this);
        Attack = new AttackPawnState(this);
    }
}
