using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PawnStateBase : StateBase
{
    protected PawnProcessor m_pawnProcessor;
    public PawnProcessor pawnProcessor => m_pawnProcessor;

    public PawnStateBase(Processor processor) : base(processor)
    {
        m_pawnProcessor = processor as PawnProcessor;
    }
}
