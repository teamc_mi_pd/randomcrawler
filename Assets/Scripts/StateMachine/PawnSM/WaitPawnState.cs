using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitPawnState : PawnStateBase   
{
    public WaitPawnState(Processor processor) : base(processor)
    {

    }

    public override void OnStateEnter()
    {
       // pawnProcessor.pawnOwner OnTakeDamage
    }

    public override void OnStateExit()
    {
      
    }

    public override void StateTick(float deltaTime)
    {
       
    }
}
