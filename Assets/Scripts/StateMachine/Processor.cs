using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Processor 
{
    private GameObject m_owner;
    public GameObject Owner => m_owner;
    protected StateBase m_currentState = null;
    public bool bDebug;

    public Processor(GameObject t_owner)
    {
        m_owner = t_owner;
    }
    public void SwitchState(StateBase t_newState)
    {
        if (t_newState == null)
        {
            return;
        }
        if (m_currentState != null)
            m_currentState.OnStateExit();
        m_currentState = t_newState;
        m_currentState.OnStateEnter();  
        //Debug
        if (bDebug)
        {
            Debug.Log(m_currentState);
        }
    }

    public void UpdateState(float deltaTime)
    {
        if (m_currentState != null)
        {
            m_currentState.StateTick(deltaTime);
            if(bDebug)
            {
                Debug.Log(m_currentState);
            }
        }
    }
}
