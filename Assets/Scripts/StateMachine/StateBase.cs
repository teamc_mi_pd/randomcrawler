using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StateBase 
{
    protected Processor m_processor;
    public StateBase(Processor processor)
    {
        m_processor = processor;
    }
    public abstract void OnStateEnter();
    public abstract void OnStateExit();
    public abstract void StateTick(float deltaTime);
}
