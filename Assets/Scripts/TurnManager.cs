using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class TurnManager
{
    private List<PlayerPawn> PlayerPawns = new List<PlayerPawn>();
    public List<PlayerPawn> playerPawns => PlayerPawns;
    private List<EnemyPawn> AIPawns = new List<EnemyPawn>();
    private int m_turnIndex = 0;

    public UnityEvent NewTurn = new UnityEvent();
    public bool bPlayerTurn = true;

    public void PawnDeath(Pawn t_pawn)
    {
        PlayerPawn Pawn = t_pawn.GetComponent<PlayerPawn>();
        EnemyPawn Enemy = t_pawn.GetComponent<EnemyPawn>();
        if (Pawn != null)
        {
            PlayerPawns.Remove(Pawn);
            if (PlayerPawns.Count <= 0)
            {
                GameMode.EndGame();
            }
        }
        else if (Enemy != null)
        {
            AIPawns.Remove(Enemy);
        }
    }
    public void AddPawnToPlayers(PlayerPawn[] t_pawns)
    {
        foreach (PlayerPawn pawn in t_pawns)
        {
            PlayerPawns.Add(pawn);
            pawn.OnTurnCompletead.AddListener(NextTurn);
        }
        GeneratePlayerTurnOrder();
    }

    public void AddPawnToAI(EnemyPawn[] t_pawns)
    {
        foreach (EnemyPawn pawn in t_pawns)
        {
            pawn.OnTurnCompletead.AddListener(NextTurn);
            AIPawns.Add(pawn);
        }
        GenerateAITurnOrder();
    }

    private void NextTurn()
    {
        m_turnIndex++;
        if (bPlayerTurn && m_turnIndex >= PlayerPawns.Count)
        {
            m_turnIndex = 0;
            if (!(AIPawns.Count <= 0))
            {
                bPlayerTurn = !bPlayerTurn;
            }
        }
        else if (!bPlayerTurn && m_turnIndex >= AIPawns.Count)
        {
            m_turnIndex = 0;
            bPlayerTurn = !bPlayerTurn;
        }
        if (!bPlayerTurn)
        {
            GameMode.ResetGrid();
            EnemyPawn Enemy = GetActivePawn() as EnemyPawn;
            Enemy.AwakePawn();
        }
        else
        {
            NewTurn?.Invoke();
        }
    }

    private void GenerateAITurnOrder()
    {
        //Bubble Sort
        EnemyPawn temp;
        bool swapped;
        for (int i = 0; i < AIPawns.Count - 1; i++)
        {
            swapped = false;
            for (int j = 0; j < AIPawns.Count - i - 1; j++)
            {
                if (AIPawns[j].TurnSpeed > AIPawns[j + 1].TurnSpeed)
                {
                    temp = AIPawns[j];
                    AIPawns[j] = AIPawns[j + 1];
                    AIPawns[j + 1] = temp;
                    swapped = true;
                }
            }
            if (swapped == false)
                break;
        }
    }
    private void GeneratePlayerTurnOrder()
    {
        //Bubble Sort
        PlayerPawn temp;
        bool swapped;
        for (int i = 0; i < PlayerPawns.Count - 1; i++)
        {
            swapped = false;
            for (int j = 0; j < PlayerPawns.Count - i - 1; j++)
            {
                if (PlayerPawns[j].TurnSpeed > PlayerPawns[j + 1].TurnSpeed)
                {
                    temp = PlayerPawns[j];
                    PlayerPawns[j] = PlayerPawns[j + 1];
                    PlayerPawns[j + 1] = temp;
                    swapped = true;
                }
            }
            if (swapped == false)
                break;
        }
    }
    public Pawn GetActivePawn()
    {
        if (bPlayerTurn)
        {
            return PlayerPawns[m_turnIndex];
        }
        else
        {
            AIPawns[m_turnIndex].SetPlayerRef(PlayerPawns.ToArray());
            return AIPawns[m_turnIndex];

        }
    }
}
