using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterButton : MonoBehaviour
{
    public  Pawn ConnectedPawn;

    private void Awake()
    {
        GetComponent<Button>().onClick.AddListener(OnClicked);
    }
    private void OnClicked()
    {
        GetComponentInParent<CharacterMenu>().SetCurrentPawn(ConnectedPawn);
    }
}
