using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor.Search;
using UnityEngine;
using UnityEngine.UI;

public class CharacterMenu : MonoBehaviour
{
    [SerializeField]
    public GameInfo GameData;
    [SerializeField]
    private ToolTip ItemToolTip;
    //UI Prefabs
    [SerializeField]
    private GameObject ButtonPrefab;
    [SerializeField]
    private GameObject ItemSelectionPrefab;
    //Interactable Item
    [SerializeField]
    private GameObject m_playerButton;
    [SerializeField]
    private Button m_readyButton;
    //Item Slot
    [SerializeField]
    private ItemSlot ChestItemSlot;
    [SerializeField]
    private ItemSlot WeaponItemSlot;
    //Stats Slider
    [SerializeField]
    private Stats m_health;
    [SerializeField]
    private Stats m_armor;
    [SerializeField]
    private Stats m_speed;
    [SerializeField]
    private Stats m_damage;


    private MyScrollRect m_ScrollView;
    private Pawn m_currentPawn;
    public Pawn CurrentPawn => m_currentPawn;
    private List<GameObject> m_chestItems = new List<GameObject>();
    private List<GameObject> m_weaponItems = new List<GameObject>();

    private void StartGame()
    {
        GameMode.StartGame();
        gameObject.SetActive(false);
        ItemToolTip.GameStart = true;
    }
    public void Init()
    {
        m_readyButton.onClick.AddListener(StartGame);
        m_ScrollView = GetComponentInChildren<MyScrollRect>();
        WeaponItemSlot.Init(this);
        ChestItemSlot.Init(this);
        int i = 0;
        foreach (Pawn pawn in GameData.GenerateCharacter())
        {
            i++;
            CharacterButton newButton = Instantiate(ButtonPrefab, m_playerButton.transform).GetComponent<CharacterButton>();
            newButton.ConnectedPawn = pawn;
            newButton.GetComponentInChildren<TMP_Text>().text = i.ToString();
            if (m_currentPawn == null)
                m_currentPawn = pawn;
        }
        UpdateStats();
        foreach (Item Equip in GameData.ChestItem)
        {
            ItemSelection Selector = Instantiate(ItemSelectionPrefab, m_ScrollView.content.transform).GetComponent<ItemSelection>();
            m_chestItems.Add(Selector.gameObject);
            Selector.SetUp(Equip, ItemToolTip);
        }
        foreach (Item Equip in GameData.WeaponItem)
        {
            ItemSelection Selector = Instantiate(ItemSelectionPrefab, m_ScrollView.content.transform).GetComponent<ItemSelection>();
            m_weaponItems.Add(Selector.gameObject);
            Selector.gameObject.SetActive(false);
            Selector.SetUp(Equip, ItemToolTip);
        }
    }
    public void ShowWeapon()
    {
        foreach (GameObject obj in m_weaponItems)
        {
            obj.SetActive(true);
        }
        foreach (GameObject obj in m_chestItems)
        {
            obj.SetActive(false);
        }
    }
    public void ShowChest()
    {
        foreach (GameObject obj in m_weaponItems)
        {
            obj.SetActive(false);
        }
        foreach (GameObject obj in m_chestItems)
        {
            obj.SetActive(true);
        }
    }
    public void SetCurrentPawn(Pawn t_pawn)
    {
        if (t_pawn != m_currentPawn)
        {
            m_currentPawn = t_pawn;
            LoadSlot(m_currentPawn);
        }
    }
    private void LoadSlot(Pawn t_pawn)
    {
        ChestItemSlot.Load(t_pawn);
        WeaponItemSlot.Load(t_pawn);
        UpdateStats();
    }
    public void UpdateStats()
    {
        m_armor.UpdateVisual(CurrentPawn.Armor);
        m_health.UpdateVisual(CurrentPawn.MaxHealth);
        m_speed.UpdateVisual(CurrentPawn.TotalSpeed);
        m_damage.UpdateVisual(CurrentPawn.Damage);
    }
}
