using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine.UI;

public class ChestSlot : ItemSlot
{
    public override void Load(Pawn t_pawn)
    {
        if (t_pawn.Chest != null && t_pawn.Chest.Icon != null)
        {
            m_equipPreview.SetActive(true);
            m_equipPreview.GetComponent<Image>().sprite = t_pawn.Chest.Icon;
        }
        else
        {
            m_equipPreview.SetActive(false);
        }
    }

    public override void ShowItems()
    {
        m_menu.ShowChest();
    }

    protected override void EquipItem(Item t_item)
    {
        if (!m_menu.GameData.ChestItem.Contains(t_item))
        {
            return;
        }
        m_equipPreview.SetActive(true);
        m_equipPreview.GetComponent<Image>().sprite = t_item.Icon;
        m_equip = t_item;
        m_menu.CurrentPawn.Chest = m_equip;
        m_menu.UpdateStats();
    }
}

