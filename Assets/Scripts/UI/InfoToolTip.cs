using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class InfoToolTip : MonoBehaviour
{
    [SerializeField]
    private RectTransform backGround;
    [SerializeField]
    private TMP_Text m_text;
    private void Update()
    {
        if (gameObject.activeSelf)
        {
            float x = Input.mousePosition.x;
            float y = Input.mousePosition.y;
            
            if (Input.mousePosition.x + backGround.rect.width > Screen.width)
            {
                x = Screen.width - backGround.rect.width;
            }
            if (Input.mousePosition.x < 0)
            {
                x = 0;
            }
            if (Input.mousePosition.y + backGround.rect.height > Screen.height)
            {
               y =  Screen.height - backGround.rect.height;
            }
            if (Input.mousePosition.y < 0)
            {
                y = 0;
            }
            transform.position = new Vector3(x, y);
        }
    }
    private void SetText(string t_text)
    {
        m_text.text = t_text;
        m_text.ForceMeshUpdate();
        Vector2 textSize = m_text.GetRenderedValues(false);
        Vector2 Padding = new Vector2(10, 10);
        backGround.sizeDelta = textSize + Padding;
    }
    public void ShowToolTip(string t_string)
    {
        gameObject.SetActive(true);
        SetText(t_string);
    }
    public void HideToolTip()
    {
        gameObject.SetActive(false);
    }
}
