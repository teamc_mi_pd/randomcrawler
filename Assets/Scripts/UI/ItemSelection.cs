using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemSelection : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler, IPointerEnterHandler, IPointerExitHandler
{
    private Item m_item;
    public Item Item => m_item;
    private ToolTip Info;
    private bool bOver;
    [HideInInspector]
    public GameObject DragPreview;

    public void OnBeginDrag(PointerEventData eventData)
    {
        DragPreview.SetActive(true);
    }

    public void OnDrag(PointerEventData eventData)
    {
        DragPreview.GetComponent<RectTransform>().position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        DragPreview.SetActive(false);
    }

    public void SetUp(Item t_item,ToolTip t_tip)
    {
        m_item = t_item;
        GetComponent<Image>().sprite = m_item.Icon;
        DragPreview = new GameObject();
        DragPreview.transform.SetParent(transform.root);
        DragPreview.AddComponent<Image>();
        DragPreview.GetComponent<Image>().raycastTarget = false;
        DragPreview.GetComponent<Image>().sprite = m_item.Icon;
        DragPreview.GetComponent<RectTransform>().sizeDelta = GetComponent<RectTransform>().sizeDelta;
        DragPreview.SetActive(false);
        Info = t_tip;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1) && bOver)
        {
            Info.ShowToolTip(m_item);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        bOver = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        bOver = false;
    }
}
