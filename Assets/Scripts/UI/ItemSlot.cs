using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEditor;

public abstract class ItemSlot : MonoBehaviour, IDropHandler
{
    protected GameObject m_equipPreview;
    protected Item m_equip;
    public Item Equip => m_equip;
    protected Button m_button;
    protected CharacterMenu m_menu;
    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag.GetComponent<ItemSelection>() != null)
        {
            EquipItem(eventData.pointerDrag.GetComponent<ItemSelection>().Item);
        }
    }

    protected abstract void EquipItem(Item t_item);

    public void Init(CharacterMenu t_menu)
    {
        m_menu = t_menu;
        m_button = GetComponent<Button>();
        m_button.onClick.AddListener(ShowItems);
        m_equipPreview = new GameObject();
        m_equipPreview.transform.SetParent(transform);
        m_equipPreview.transform.localPosition = new Vector3(0, 0, 0);
        m_equipPreview.AddComponent<Image>();
        m_equipPreview.GetComponent<Image>().raycastTarget = false;
        m_equipPreview.GetComponent<RectTransform>().sizeDelta = GetComponent<RectTransform>().sizeDelta;
        m_equipPreview.SetActive(false);
    }
    public abstract void Load(Pawn t_pawn);
    public abstract void ShowItems();

}
