using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private Slider m_slider;
    [SerializeField]
    private Button m_button;
    public Button Button => m_button;
    [SerializeField]
    private TMP_Text m_text;
    [SerializeField]
    private GameInfo m_gameInfo;

    private void Start()
    {
        m_slider.onValueChanged.AddListener(OnSliderChange);
        m_text.text = m_slider.minValue.ToString();
        m_gameInfo.NumberOfPlayer = Mathf.FloorToInt(m_slider.minValue);
    }

    private void OnSliderChange(float t_value)
    {
        m_text.text = t_value.ToString();
        m_gameInfo.NumberOfPlayer = Mathf.FloorToInt(t_value);

    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
