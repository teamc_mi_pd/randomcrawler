using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MyScrollRect : ScrollRect
{
    private bool m_IsDragging = true;
    public void StopBar()
    {
        m_IsDragging = false;
    }
    public void ResumeMovement()
    {
        m_IsDragging = true;
    }
    public override void OnBeginDrag(PointerEventData eventData)
    {
        if (m_IsDragging)
        {
            base.OnBeginDrag(eventData);
        }
    }
    public override void OnDrag(PointerEventData eventData)
    {
        if(m_IsDragging)
        {
            base.OnDrag(eventData);
        }
    }
    public override void OnEndDrag(PointerEventData eventData)
    {
        if (m_IsDragging)
        {
            base.OnEndDrag(eventData);
        }
    }
}
