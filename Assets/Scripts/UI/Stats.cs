using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Stats : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField, Multiline]
    private string m_toolTipString;
    [SerializeField]
    private InfoToolTip m_toolTip;
    [SerializeField]
    private Image Fill;
    [SerializeField]
    private TMP_Text Value;
    [SerializeField]
    float MaxRange;

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (m_toolTip != null)
            m_toolTip.ShowToolTip(m_toolTipString);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (m_toolTip != null)
            m_toolTip.HideToolTip();
    }

    public void UpdateVisual(float t_value)
    {
        Value.text = t_value.ToString();
        Fill.fillAmount = t_value / MaxRange;
    }
}
