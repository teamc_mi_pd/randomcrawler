using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using UnityEngine.EventSystems;

public class ToolTip : MonoBehaviour, IPointerExitHandler
{
    [SerializeField]
    Vector3 OffSet;
    [SerializeField]
    Camera Cam;
    public bool GameStart;
    private Vector3 m_location;
    [SerializeField]
    private Stats HP;
    [SerializeField]
    private Stats Armor;
    [SerializeField]
    private Stats Speed;
    [SerializeField]
    private Stats Damage;

    public void ShowTooltip(Pawn pawn, Vector3 t_location)
    {
        m_location = t_location;
        UpDateStates(pawn);
        transform.position = Input.mousePosition;
        gameObject.SetActive(true);
    }
    public void ShowToolTip(Item t_item)
    {
        UpDateStates(t_item);
        transform.position = Input.mousePosition - new Vector3(0,10);
        gameObject.SetActive(true);
    }

    private void Update()
    {
        if(gameObject.activeSelf && GameStart)
        {
            transform.position = Cam.WorldToScreenPoint(m_location+OffSet);
        }
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }
    private void UpDateStates(Pawn t_pawn)
    {
        HP.UpdateVisual(t_pawn.Health);
        Armor.UpdateVisual(t_pawn.Armor);
        Damage.UpdateVisual(t_pawn.Damage);
        Speed.UpdateVisual(t_pawn.TotalSpeed);
    }
    private void UpDateStates(Item t_item)
    {
        HP.UpdateVisual(t_item.Health);
        Armor.UpdateVisual(t_item.Armor);
        Damage.UpdateVisual(t_item.Damage);
        Speed.UpdateVisual(t_item.Speed);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        gameObject.SetActive(false);
    }
}
