using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private MainMenu m_mainMenu;
    [SerializeField]
    private CharacterMenu m_characterMenu;

    private void Awake()
    {
        m_mainMenu.Button.onClick.AddListener(OnPlayClicked);
    }
    private void OnPlayClicked()
    {
        m_mainMenu.Hide();
        m_characterMenu.Init();
    }
}
