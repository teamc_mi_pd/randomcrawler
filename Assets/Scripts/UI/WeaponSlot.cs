using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class WeaponSlot : ItemSlot
{
    public override void Load(Pawn t_pawn)
    {
        if (t_pawn.MainHand != null && t_pawn.MainHand.Icon)
        {
            m_equipPreview.SetActive(true);
            m_equipPreview.GetComponent<Image>().sprite = t_pawn.MainHand.Icon;
        }
        else
        {
            m_equipPreview.SetActive(false);
        }
    }

    public override void ShowItems()
    {
        m_menu.ShowWeapon();
    }

    protected override void EquipItem(Item t_item)
    {
        if (!m_menu.GameData.WeaponItem.Contains(t_item))
        {
            return;
        }
        m_equipPreview.SetActive(true);
        m_equipPreview.GetComponent<Image>().sprite = t_item.Icon;
        m_equip = t_item;
        m_menu.CurrentPawn.MainHand = m_equip;
        m_menu.UpdateStats();
    }
}
